# Diplo

## Materia: Gestión de la calidad y ciclo de vida del desarrollo de software

### Consigna trabajo final

<img src="https://i.imgur.com/TzZDSg4.png" align="center"></img>

### Grupo 2

---

### Checkpoint 1: 

```
[ ] - Crear un repositorio y diagramarlo con esquema de ramas por entorno (grupos pares).

[ ] - Desafio posterior: Diagramar la estrategia de branching con ramas por features (gitflow)
[ ] - Desafio adicional: Generar un merge conflict
```

#### Comandos de ayuda
```
git log --all --decorate --oneline --graph
git checkout -b(ranch) <nombre>
```

#### Recursos adicionales

- [documentacion oficial git](https://www.git-scm.com/doc)
- [juego "learning git branching"](https://learngitbranching.js.org/?locale=es_AR)
- [Estrategia de Ramas por Entorno](https://www.linkedin.com/pulse/estrategia-de-ramas-por-entorno-environment-based-branching-matías-c/)
- [Otro link de estrategia de ramas por entorno](https://jesuslc.com/2015/12/30/estrategias-de-branching-no-solo-existe-git-flow/)
- [Opiniones sobre estrategias de branchs/workflows para despliegue de multiples entornos](https://old.reddit.com/r/devops/comments/16mqdx2/branch_workflow_strategies_for_automated_deploys/)

